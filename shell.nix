with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "env";
  buildInputs = [
    ruby.devEnv
    git
    sqlite
    libpcap
    postgresql
    libxml2
    libxslt
    pkg-config
    bundix
    gnumake
  ];

  PROJECT_ROOT = builtins.toString ./.;
  GEM_HOME = "${PROJECT_ROOT}/.gems";
  GEM_PATH = "${PROJECT_ROOT}/.gems";

  shellHook = ''
	export PATH="$PROJECT_ROOT/.gems/bin:$PATH"
  '';
}
