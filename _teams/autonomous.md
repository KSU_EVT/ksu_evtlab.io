---
layout: page
title: Autonomous Division
feature-img: "assets/img/teams/autonomous_banner.jpg"
img: "assets/img/teams/autonomous_icon.jpg"
date: 27 September 2015
tags: [Lorem, Ipsum, Portfolio]
---
This year, the autonomous division of EVT is a new part of the organization. This year the team has the goal of taking first place in the autonomous division at the [EvGrandPrix](https://evgrandprix.org/autonomous/) during mid-May of 2020. Our goals are three fold: 1. Ground-up re-design of software architecture built around Ros2 and B-Spline SLAM, 2. Be built around safety and reliability, and 3. Achieve a thoroughly tested and reliable system. The team has its own Velodyne VLP-16 lidar, as well as two smaller lidar sensors being a YDlidar and an RPlidar which are used on a small-scale test platform. In order to develop the software architecture the team will be using a gazebo simulator to do full and partial stack testing with model-based motion control and fully simulated sensor input. 
			
In order to accomplish these goals, the first and foremost have in mind the team of people behind said kart. For this season, the team has learned from our previous experiences and have so far developed a detailed plan with goals ready for members to start working towards. As a division, autonomous for the 2019-2020 season is extremely excited for the chance to make its first real debut as the newest competition group at KSU under the EVT organization. If you would like more information on the project and team, feel free to contact the autonomous lead for all questions and comments.

