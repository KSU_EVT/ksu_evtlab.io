---
layout: page
title: About
permalink: /about/
feature-img: "assets/img/about/about_banner.jpg"
tags: [About, Archive]
gallery_path: "assets/img/gallery"
pagination: 
  enabled: true
---

The Kennesaw State University Electric Vehicle Team is an organization that designs, builds, and races electric vehicles for competition. EVT is committed to being new-member oriented with new classes teaching everything required to start working as soon as possible and to working as efficiently as possible. A new member is asked not to be knowledgeable but instead able to commit. If you want more information feel free to contact us using the [form](/evt-website/contact/index.html), join our [discord](https://discord.gg/hDpMvW8) and if you are ready to join head over to our [Owl Life](https://owllife.kennesaw.edu/organization/EVT)
 

{% include teams.html %}

<br/>
<br/>
<br/>
<br/>

<h3>2018 - 2019 Season in Pictures</h3>

{% include gallery.html gallery_path=page.gallery_path %}