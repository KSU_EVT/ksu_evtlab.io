---
layout: post
title: Week One Time to Recruit
tags: []
author-id: mdavenport
hide_title: false
---
Hope everyone has had a good syllabus week so far! For EVT things have been ramping up over the weekend and first two days of class. We have given several prospective members shop tours already with some already beginning their first projects! We are prepping our budget proposal for the Polytechnic Foundation for funding this week and prepping for our events that are coming up on campus that you may catch us attending detailed below.
![Booth](/assets/img/posts/2019-08-19-First-Week/booth.png){:class="img-responsive"} ![HootyHoo](/assets/img/posts/2019-08-19-First-Week/hootyhoo.png)

To start off, we will be attending involvement fairs on the Marietta and Kennesaw campus will take place the 21st and 22nd. This fair will be a chance for you to ask questions and talk with members of the team if you are not ready to commit to the team yet. The next event will be the Competition test drive event on the 4th of September which will showcase our competition teams and their projects in person on the Marietta campus. The event is open to the public and will provide food and drinks. If you are interested in joining KSU EVT, the new member meeting will take place on the 28th at 8pm in Q202. To start off the next month on Sept 4th, we will be hosting our start of year cookout. We encourage everyone to come including freshman, members of other teams, alumni, and prospective members. This month will be the beginning of a great year.
